function validaLogin(){
    let userTxt = localStorage.getItem("userLogged");
    if(!userTxt) {
        window.location = "index.html";
    }
}
function logout(){
    localStorage.removeItem("userLogged");
    window.location = "index.html";
}
function voltar(){    
    window.location = "dashmenu.html";
}

function gerarRelatorioEventos(){    
    let dataini = document.getElementById("dtinicio").value;
    let datafim = document.getElementById("dtfinal").value;

    let dataMsg = {
        dt1: dataini,
        dt2: datafim
    }

    let cabecalho ={
        method: 'POST',
        body: JSON.stringify(dataMsg),
        headers: {
            'Content-type': 'application/json'
        }
    }
    fetch("http://localhost:8080/evento/data", cabecalho)
        .then(res => res.json()) //extrai os dados do retorno
        .then(result => preencheEventos(result));
}

function preencheEventos(dados){
    let tabela = '<table class="table table-sm"> <tr><th>Data</th> <th>Alarme</th> <th>Equipamento</th></tr>';
    for (i =0; i < dados.length; i++) {
        tabela += `<tr> 
                    <td>${new Date(dados[i].dataevt).toLocaleString("pt-BR")}</td>
                    <td>${dados[i].alarme.nome}</td>
                    <td>${dados[i].equipamento.hostname}</td>                    
                    </tr>`;
    }
    tabela = tabela+'</table>';
    document.getElementById("tabela").innerHTML=tabela;
}