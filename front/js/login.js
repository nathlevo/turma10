function autenticar(event) {
    event.preventDefault(); //evita o comportamento padrão, neste caso de enviar o formulário com os dados
    let txtUsuario = document.getElementById("txtUser");
    let txtSenha = document.getElementById("txtSenha");
    let loginMsg = {
        email: txtUsuario.value,
        racf: txtUsuario.value,
        senha: txtSenha.value
    }
    let cabecalho = {
        method: 'POST',
        body: JSON.stringify(loginMsg),
        headers: {
            'Content-type': 'application/json'
        }
    }
    fetch("http://localhost:8080/login", cabecalho) //envia para o back a msg para login
        .then(res => tratarResposta(res)); //quando o fetche receber a resposta 'res' então ...
}

function tratarResposta(resposta) {
    if (resposta.status == 200) {
        resposta.json().then(res => fazerLogin(res));
    } else {
        document.getElementById("msgError").innerHTML = "Usuário/Senha Inválido";
    }
}

function fazerLogin(user) {
    localStorage.setItem("userLogged",JSON.stringify(user));
    window.location = "dashmenu.html";
}