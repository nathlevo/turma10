package exercicio;

import java.util.Scanner;

public class Exercicio05 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Informe o custo do veículo: ");
        double custo , distribuidor , imposto, total;
        custo = entrada.nextDouble();
        distribuidor = custo * 0.28;
        imposto = custo * 0.45;
        total = custo + distribuidor + imposto;
        System.out.printf("O custo final ao consumidor é R$ %.2f, sendo R$ %.2f de imposto.",total,imposto);
        entrada.close();
    }
}
