package exercicio;
/*
    Exibir o resultado das 4 operações aritméticas básicas: + - * /
    entre os valores 15 e 7
*/
public class Exercicio01 {
    public static void main(String[] args) {
        System.out.println("15 + 7 = " + (15 + 7));
        System.out.println("15 - 7 = " + (15 - 7));
        System.out.println("15 * 7 = " + (15 * 7));
        System.out.println("15 / 7 = " + (15 / 7));
        System.out.println("15 / 7 = " + (15.0 / 7.0));
        System.out.println("15 % 7 = " + (15 % 7));
    }
}
