package exercicio;
import java.util.Scanner;
/*
    Faça um programa que leia o salário de um funcionário, Sabendo que o salário teve aumento de 25%. Calcular novo
*/
public class Exercicio03 {
    public static void main(String[] args) {        
        Scanner entrada = new Scanner(System.in);
        double salario;

        System.out.println("Informe o salário atual: ");     
        salario = entrada.nextDouble();

        System.out.println("O salário com aumento de 25% é: " + (salario+(salario*25)/100) );
        
        entrada.close();
    }
}