package exercicio;
import java.util.Scanner;
/*
    1 kw 1/500 avos do salário minimo
    receba valor do sal. min. e #kw consumido
    Exiba: 1. Valod do kw 2. valor a ser pago 3. Valor com 15% de desconto
*/
public class Exercicio04 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        double salario_min, consumo; //entrada
        double valor_kw, valor_a_pagar, valor_a_pagar_com_desconto; //saídas

        System.out.println("Informe o salário mínimo atual: ");
        salario_min = entrada.nextDouble();

        System.out.println("Informe o consumo em kw: ");
        consumo = entrada.nextDouble();

        valor_kw = salario_min/500;
        valor_a_pagar = valor_kw * consumo;
        valor_a_pagar_com_desconto = valor_a_pagar * 0.85;

        System.out.printf("\n\nO valor do kw é R$ %.2f" , valor_kw);
        System.out.printf("\nO valor da conta é R$ %.2f" , valor_a_pagar);
        System.out.printf("\nO valor com desconto é R$ %.2f\n\n" , valor_a_pagar_com_desconto);

        entrada.close();
        
    }
}
