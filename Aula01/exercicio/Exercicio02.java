package exercicio;

import java.util.Scanner;

/*
    Faça um programa que leia duas notas de um aluno, calcule e mostre a média aritmética
*/
public class Exercicio02 {
    public static void main(String[] args) {        
        Scanner entrada = new Scanner(System.in);
        System.out.println("Digite a primeira nota: ");
        float nota1,nota2;
        nota1 = entrada.nextFloat();
        System.out.println("Digite a segunda nota: ");
        nota2 = entrada.nextFloat();
        System.out.println("A média das notas é: " + (nota1+nota2)/2 );
        entrada.close();
    }
}