package exercicio;

import java.util.Scanner;

public class Exercicio06 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        double medidaKm, medidaPolegada, medidaPe, medidaJarda, medidaMilha;

        System.out.println("Informe a medida em km: ");
        medidaKm = teclado.nextDouble();
        
        medidaPolegada = 12*3*1.760*0.62137*medidaKm;
        medidaPe = 3*1.760*0.62137*medidaKm;
        medidaJarda = 1.760*0.62137*medidaKm;
        medidaMilha =0.62137*medidaKm;

        System.out.printf("%.2f km\n%.2f milhas\n%.2f jardas\n%.2f pés\n%.2f polegadas\n",medidaKm, medidaMilha, medidaJarda, medidaPe, medidaPolegada);

        teclado.close();
    }
}
