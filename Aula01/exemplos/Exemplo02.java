package exemplos;

public class Exemplo02 {

    public static void main(String[] args) { //run acima ou [CTRL + F5] para executar
        //println: imprime em uma nova linha
        System.out.println("Um texto qualquer.1"); //"": Um texto
        System.out.println("Outro texto qualquer.\nNathalia2"); //\n quebra de linha

        //print: imprime na linha em que estava
        System.out.print("Um texto qualquer.3"); //"": Um texto
        System.out.print("Outro texto qualquer.\nNathalia4"); //\n quebra de linha

    } 
}
