package exemplos; //indica a pasta onde a classe foi criada, a partir da raiz do projeto
/**
 * Exemplo01
 * @author: Nathalia Levorato Varela
 */
  /*
    Este é um comentário de bloco. Envolve várias linhas de código
 */
 // Este é um comentário de linha
public class Exemplo01 {
    public static void main(String[] args) { //atalho: main
        //[CTRL + S] : Salvar
        //[CTRL + K] + S : Salvar
        System.out.println("Meu Primeiro Programa!"); //atalho: syso --> Saída padrão do sistema (tela)
    }
}