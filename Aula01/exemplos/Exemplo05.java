package exemplos;

import java.util.Scanner;

public class Exemplo05 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Digite um número: ");
        int numero;
        numero = entrada.nextInt();
        System.out.println("O número digitado é: " + numero);
        entrada.close();
    }
}
