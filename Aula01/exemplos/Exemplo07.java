package exemplos;

import java.util.Scanner;

public class Exemplo07 {
    public static void main(String[] args) {
        final double KM_TO_POLEGADA , KM_TO_PE , KM_TO_JARDA , KM_TO_MILHA;
        KM_TO_POLEGADA = 12*3*1.760*0.62137;
        KM_TO_PE = 3*1.760*0.62137;
        KM_TO_JARDA = 1.760*0.62137;
        KM_TO_MILHA =0.62137;
        double medidaKm;
        
        Scanner teclado = new Scanner(System.in);
        System.out.println("Informe a medida em km: ");
        medidaKm = teclado.nextDouble();


        System.out.printf("%.2f km\n%.2f milhas\n%.2f jardas\n%.2f pés\n%.2f polegadas\n", medidaKm, medidaKm*KM_TO_MILHA , medidaKm*KM_TO_JARDA , medidaKm*KM_TO_PE , medidaKm*KM_TO_POLEGADA);
        teclado.close();
    }
}
