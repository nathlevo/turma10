package exemplos;

public class Exemplo06 {
    public static void main(String[] args) {
        String nome = "Carlos";
        int idade = 34;
        double peso = 85.321;
        System.out.println("Nome: " + nome + "\nIdade: "+ idade + "\nPeso: " + peso);
        /*
            d: inteiro (byte, short, int, long)
            f: float/double %.2f
            C: Character
            S: String
            h: hascode
            n: newline (%n ou \n)
        */
        System.out.printf("\n\nNome: %s\nIdade: %d\nPeso: %.2f",nome,idade,peso);
    }
}
