package exemplos;

// Entrada --> Processamento --> Saída

public class Exemplo04 {
    public static void main(String[] args) {
        int numero; //cria variável para armazenar valor inteiro
        numero = 10; //armazena valor 10 dentro da variável
        System.out.println(numero); //imprime o valor da variável
        numero = 5 ;
        System.out.println(numero);
        System.out.println(numero + 7);
        System.out.println(numero);
        numero = 4 - numero;
        System.out.println(numero);
    }
}
