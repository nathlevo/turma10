package exemplos;

public class Exemplo03 {
    public static void main(String[] args) {
        System.out.println("Resposta: " + 100); // + significa concatenar
        System.out.println("Resposta: " + 10 + 20); // + significa concatenar
        System.out.println("Resposta: " + (10 + 20)); // + significa concatenar, quando entre parentese indica que é uma conta
        System.out.println("Resposta: " + 10 * 20); // + significa concatenar, quando entre parentese indica que é uma conta
        //Operadores aritméticos na sequência de prioridade: * / + -
    }
}
