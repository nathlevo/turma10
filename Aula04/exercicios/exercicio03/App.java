package exercicios.exercicio03;

public class App {
    public static void main(String[] args) {
        Veiculo veiculo01 = new Veiculo("Palio", "Fiat", 6.00);
        Veiculo veiculo02 = new Veiculo("Ka", "Ford", 9.00);
        veiculo01.dadosVeiculo();
        System.out.println("O consumo é "+veiculo01.consumoVeiculo()+" Km/l");
        System.out.println();
        veiculo02.dadosVeiculo();
        System.out.println("O consumo é "+veiculo02.consumoVeiculo()+" Km/l");
    }
}
