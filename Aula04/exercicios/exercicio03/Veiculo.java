package exercicios.exercicio03;

public class Veiculo {
    String modelo, marca;
    Double consumo;
    public Veiculo(String modelo, String marca, Double consumo){
        this.modelo=modelo;
        this.marca=marca;
        this.consumo=consumo;
    }

    void dadosVeiculo (){
        System.out.println("Modelo: "+modelo);
        System.out.println("Marca: "+marca);
    }

    double consumoVeiculo(){
        return consumo;
    }
}
