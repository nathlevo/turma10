package exercicios;

import java.util.Scanner;

/**
 * Exercicio01
 */
public class Exercicio02 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Informe o 1º número: ");
        int a1 = entrada.nextInt();
        System.out.print("Informe o 2º número: ");
        int b1 = entrada.nextInt();
        System.out.print("Informe o 3º número: ");
        int c1 = entrada.nextInt();

        System.out.println("O menor número informado é: " + menor(a1, b1, c1));
        entrada.close();
    }

    static int menor(int a, int b, int c) {
        if (a < b && a < c) {
            return a;
        } else {
            if (b < c) {
                return b;
            } else {
                return c;
            }
        }

    }
}