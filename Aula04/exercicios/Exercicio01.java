package exercicios;

import java.util.Scanner;



/**
 * Exercicio01
 */
public class Exercicio01 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner (System.in);
        System.out.println("Informe um número: ");
        int inteiro = entrada.nextInt();
        System.out.println("O número informado é par." + EPar(inteiro));
        entrada.close();
    }

    static boolean EPar(int inteiro) {
        Boolean ePar = false;
        if (inteiro % 2 == 0) {
            ePar =true;
        }
        return ePar;
    }
}