package exercicios.exercicio04;

public class App {
    public static void main(String[] args) {
        Relogio hora0 = new Relogio(12, 59, 50);
        //System.out.println(hora0.exibirHora());
        for (int i = 0; i < 100 ; i++) {
            hora0.avancaHora();
            System.err.println(hora0.exibirHoraAMPM());
        }
        hora0.retrocedeHora();
        //System.err.println(hora0.exibirHora());
    }
}
