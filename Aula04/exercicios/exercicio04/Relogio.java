package exercicios.exercicio04;

public class Relogio {
    private int horas, minutos, segundos;

    public Relogio(int horas, int minutos, int segundos) {
        setHoras(horas);
        setMinutos(minutos);
        setSegundos(segundos);
    }

    public String exibirHora() {

        return String.format("%02d", this.horas) + ":" + String.format("%02d", this.minutos) + ":"
                + String.format("%02d", this.segundos);
    }

    public String exibirHoraAMPM() {
        if (horas >= 13 ) {
            return String.format("%02d", (this.horas - 12)) + ":" + String.format("%02d", this.minutos) + ":"
                    + String.format("%02d", this.segundos) + " PM";
        } else {
            return String.format("%02d", this.horas) + ":" + String.format("%02d", this.minutos) + ":"
                    + String.format("%02d", this.segundos) + " AM";
        }
    }

    public void setHoras(int horas) {
        if (horas >= 0 && horas < 24) {
            this.horas = horas;
        }
    }

    public void setMinutos(int minutos) {
        if (minutos >= 0 && minutos < 60) {
            this.minutos = minutos;
        }
    }

    public void setSegundos(int segundos) {
        if (segundos >= 0 && segundos < 60) {
            this.segundos = segundos;
        }
    }

    public void avancaHora() {
        segundos++;
        if (segundos == 60) {
            segundos = 0;
            minutos++;
            if (minutos == 60) {
                minutos = 0;
                horas++;
                if (horas == 24) {
                    horas = 0;
                }
            }
        }

    }

    void retrocedeHora() {
        segundos--;
        if (segundos == -1) {
            segundos = 59;
            minutos--;
            if (minutos == -1) {
                minutos = 59;
                horas--;
                if (horas == -1) {
                    horas = 23;
                }
            }
        }
    }
}
