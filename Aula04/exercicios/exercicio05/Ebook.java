package exercicios.exercicio05;

import java.util.Scanner;

public class Ebook {
    String titulo, autor;
    int totalPaginas, paginaAtual;

    public Ebook(String titulo, String autor, int totalPaginas, int paginaAtual) {
        this.titulo = titulo;
        this.autor = autor;
        this.totalPaginas = totalPaginas;
        this.paginaAtual = paginaAtual;
    }

    public void avancarPagina() {
        if (paginaAtual == totalPaginas) {
            System.out.println("Não é possível avançar.");
        } else {
            paginaAtual++;
        }
    }

    public void retrocederPagina() {
        if (paginaAtual == 0) {
            System.out.println("Não é possível recuar. Você está na capa");
        } else {
            paginaAtual--;
        }
    }

    public void irParaPag(int pagina) {
        if (pagina >= 0 && pagina <= totalPaginas) {
            paginaAtual = pagina;
        } else {
            System.out.println("Página inválida.");
        }

    }

    public int exibirPag() {
        return paginaAtual;
    }

    public void mostrarCapa() {
        System.out.println("Título: " + titulo);
        System.out.println("Autor: " + autor + "(" + totalPaginas + ")");
        paginaAtual = 0;
    }

    public void Menu() {
        boolean a = false;
        Scanner entrada = new Scanner(System.in);
        while (a == false) {
            System.out.println("\n\n1. Avançar Pagina.");
            System.out.println("2. Retroceder Pagina.");
            System.out.println("3. Ir para Página...");
            System.out.println("4. Exibir Página.");
            System.out.println("5. Mostrar Capa.");
            System.out.println("x. Sair.");
            System.out.println("Informe a opção desejada");

            
            int opcao = entrada.nextInt();
            System.out.println();
            System.out.println();
            if (opcao == 1) {
                System.out.println("Você está na: " + exibirPag());
                avancarPagina();
                System.out.println("Você está na: " + exibirPag());
            } else {
                if (opcao == 2) {
                    System.out.println("Você está na: " + exibirPag());
                    retrocederPagina();
                    System.out.println("Você está na: " + exibirPag());
                } else {
                    if (opcao == 3) {
                        System.out.print("Informar página: ");
                        irParaPag(entrada.nextInt());
                        System.out.println("Você está na: " + exibirPag());
                    } else {
                        if (opcao == 4) {
                            System.out.println("Você está na: " + exibirPag());
                        } else {
                            if (opcao == 5) {
                                mostrarCapa();
                            } else {
                                a = true;
                            }
                        }
                    }
                }
            }            
        }
        entrada.close();
    }

}
