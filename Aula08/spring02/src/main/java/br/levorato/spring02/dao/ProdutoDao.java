package br.levorato.spring02.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.levorato.spring02.model.Produto;
/*
    CRUD - Create Read Update Delete
    Utilizar o JPA para as operações básicas no BD
*/
public interface ProdutoDao extends  CrudRepository<Produto,Integer> {
    ArrayList<Produto> findByNomeLike(String nome);    
    ArrayList<Produto> findByValorLessThan(double valor);
    
    @Query(value = "Select new Produto(p.codigo, p.nome, p.valor) from Produto p Where p.codigo = :cod")   
    public Produto buscarProdutoPorId(@Param("cod") Integer codigo);

    @Query(value = "Select nome, valor From produto where valor = :valor", nativeQuery = true)    
    public Object[] buscarProdutosPorValor(@Param("valor") Double valor);
}
