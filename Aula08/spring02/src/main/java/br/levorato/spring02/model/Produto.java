package br.levorato.spring02.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Produto
 */

@Entity
@Table(name="produto")
public class Produto {
    @Id //chave primária na tabela
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Gera números sequenciais (1,2,3,...)
    
    @Column(name = "cod") //nome da coluna no BD
    private int codigo;
    
    @Column(name = "nome", length = 100, nullable = false)
    private String nome;

    @Column(name = "valor")
    private double valor;

    @ManyToOne
    @JoinColumn(name = "cod_cliente")//Nome da chave estrageira
    @JsonIgnoreProperties("produtos")
    private Cliente cliente;//No BD seria cod cliente (atributo), aqui será o objeto

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Produto() {
    }

    public Produto(int codigo, String nome, double valor) {
        this.codigo = codigo;
        this.nome = nome;
        this.valor = valor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}