package br.levorato.spring02.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.levorato.spring02.dao.ProdutoDao;
import br.levorato.spring02.model.Produto;

@RestController
@CrossOrigin("*")

public class ProdutoController {
    @Autowired // injecao de dependencia: JPA criar a classe e o objeto
    private ProdutoDao dao;

    /*
     * @GetMapping("/hello") public String hello(){ return "Funcionou!"; }
     */
    @GetMapping("/produtos")
    public ResponseEntity<ArrayList<Produto>> obterTodos() {
        ArrayList<Produto> lista = (ArrayList<Produto>) dao.findAll();
        if (lista != null) {
            return ResponseEntity.ok(lista); // ok - status 200
        } else {
            return ResponseEntity.notFound().build(); // status 404
        }
    }

    @GetMapping("/produto/{cod}")
    public ResponseEntity<Produto> obterProdutoPorCodigo(@PathVariable int cod) {
        Produto produto = dao.findById(cod).orElse(null);// Busca a chave primaria
        //Produto produto = dao.buscarProdutoPorId(cod);
        
        if (produto != null) {
            return ResponseEntity.ok(produto); // ok - status 200
        } else {
            return ResponseEntity.notFound().build(); // status 404
        }
        
    }

    @PostMapping("/produto/novo")
    public ResponseEntity<Produto> novoProduto(@RequestBody Produto produto) {
        try {
            Produto novoProduto = dao.save(produto);
            return ResponseEntity.ok(novoProduto); // ok - status 200
        } catch (Exception e) {
            // return ResponseEntity.badRequest().build();
            return ResponseEntity.status(400).build();
        }

    }

    @PostMapping("/produto/atualiza")
    public ResponseEntity<Produto> atualizaProduto(@RequestBody Produto produto) {
        try {
            if (produto.getCodigo() > 0) {
                Produto atualizadoProduto = dao.save(produto);
                return ResponseEntity.ok(atualizadoProduto); // ok - status 200
            }
            return ResponseEntity.status(400).build();

        } catch (Exception e) {
            // return ResponseEntity.badRequest().build();
            return ResponseEntity.status(400).build();
        }
    }

    @GetMapping("/produtos/nome/{nome}")
    public ResponseEntity<ArrayList<Produto>> obterProdutoPorNome(@PathVariable String nome) {
        ArrayList<Produto> lista = dao.findByNomeLike(nome);// Busca a chave primaria
        if (lista != null) {
            return ResponseEntity.ok(lista); // ok - status 200
        } else {
            return ResponseEntity.notFound().build(); // status 404
        }
    }

    @GetMapping("/produtos/valor/menor/{valor}")
    public ResponseEntity<ArrayList<Produto>> obterProdutoValorMenor(@PathVariable double valor) {
        ArrayList<Produto> lista = dao.findByValorLessThan(valor);// Busca a chave primaria
        if (lista != null) {
            return ResponseEntity.ok(lista); // ok - status 200
        } else {
            return ResponseEntity.notFound().build(); // status 404
        }
    }

    @GetMapping("/produtos/valor/{valor}")
    public ResponseEntity<Object[]> obterProdutoValorNativo(@PathVariable double valor) {
        Object[] lista = dao.buscarProdutosPorValor(valor);// Busca a chave primaria
        if (lista != null) {
            return ResponseEntity.ok(lista); // ok - status 200
        } else {
            return ResponseEntity.notFound().build(); // status 404
        }
    }


}
