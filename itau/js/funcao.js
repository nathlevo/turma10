function mensagem(){
    alert("Nossa como você é curioso!");
}

function validar(){
    var nome=form_usuario.nome.value;
    var email=form_usuario.email.value;
    var senha=form_usuario.senha.value;

    if (nome == "") {
        alert("Preencha o Campo Nome, pois é obrigatório");
        form_usuario.nome.focus();
        return false;        
    }

    if (email == "" || email.indexOf("@")==-1) {
        alert("Preencha o Campo E-mail, pois é obrigatório");
        form_usuario.email.focus();
        return false;        
    }
    
    if (senha.length<=5) {
        alert("Preencha o Campo Senha com, no mínimo, 6 caracteres.");
        form_usuario.senha.focus();
        return false;        
    }
    alert("Bem vindo(a) "+nome+". Enviaremos mais informações no e-mail "+email+" .");


}