package exemplos;

import java.util.Scanner;

public class Exemplo05 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int idade;
        String nome;

        System.out.println("Digite sua idade: ");
        idade = entrada.nextInt();
        entrada.nextLine(); //Necessário para não ler só idade

        //OU
        idade = Integer.parseInt(entrada.nextLine());
        System.out.println("Digite seu nome: ");
        nome = entrada.nextLine();

        
        System.out.println(nome + ", " + idade);

        entrada.close();

    }
}
