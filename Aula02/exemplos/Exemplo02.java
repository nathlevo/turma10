package exemplos;

public class Exemplo02 {
    public static void main(String[] args) {
        int numeroInt;
        double numeroDouble;

        //numeroInt=10;//Só aceita inteiro, não aceita decimais
        numeroInt= (int) 10.7;//Só aceita inteiro, não aceita decimais

        numeroDouble=12.5; //Aceita inteiro

        System.out.println(numeroInt);
        System.out.println(numeroDouble);
    }
}
