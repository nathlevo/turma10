package exemplos;

import java.util.Scanner;

public class Exemplo04 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        String nome;

        //nome=entrada.next();//lê até espaço
        nome=entrada.nextLine();//lê a linha inteira
        
        System.out.println(nome);

        entrada.close();
    }
}
