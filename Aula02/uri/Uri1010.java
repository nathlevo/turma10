package uri;
import java.util.Scanner;
public class Uri1010 {
    public static void main(String[] args) {
        
        double valor1, valor2, valorTotal;
        int codigo1, quantidade1, codigo2, quantidade2;
        Scanner teclado = new Scanner(System.in);
        
        codigo1 = teclado.nextInt();
        quantidade1 = teclado.nextInt();
        valor1 = teclado.nextDouble();
        codigo2 = teclado.nextInt();
        quantidade2 = teclado.nextInt();
        valor2 = teclado.nextDouble();       

        valorTotal = quantidade1*valor1 + quantidade2*valor2;

        System.out.printf("VALOR A PAGAR: R$%.2f\n", valorTotal);

        teclado.close();
    }
}
