package uri;
import java.util.Scanner;
public class Uri1018 {
    public static void main(String[] args) {
        long a;
        Scanner teclado = new Scanner(System.in);
        long nota100, nota050, nota020, nota010, nota005, nota002, nota001;
        a = teclado.nextLong();

        nota100= a/100;
        nota050 = (a - nota100 * 100 ) /50 ;
        nota020 = (a - nota100 * 100 - nota050 * 50 ) /20 ;
        nota010 = (a - nota100 * 100 - nota050 * 50 - nota020 * 20 ) /10 ;
        nota005 = (a - nota100 * 100 - nota050 * 50 - nota020 * 20 - nota010 * 10 ) /5 ;
        nota002 = (a - nota100 * 100 - nota050 * 50 - nota020 * 20 - nota010 * 10 - nota005 * 5) / 2 ;
        nota001 = (a - nota100 * 100 - nota050 * 50 - nota020 * 20 - nota010 * 10 - nota005 * 5) % 2 ;

        System.out.printf("%d", a);
        System.out.printf("\n%d nota(s) de R$ %.2f", nota100, 100.00);
        System.out.printf("\n%d nota(s) de R$ %.2f", nota050, 50.00);
        System.out.printf("\n%d nota(s) de R$ %.2f", nota020, 20.00);
        System.out.printf("\n%d nota(s) de R$ %.2f", nota010, 10.00);
        System.out.printf("\n%d nota(s) de R$ %.2f", nota005, 5.00);
        System.out.printf("\n%d nota(s) de R$ %.2f", nota002, 2.00);
        System.out.printf("\n%d nota(s) de R$ %.2f", nota001, 1.00);

        teclado.close();
    }
}
