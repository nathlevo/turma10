package uri;

import java.util.Scanner;

public class Uri1051 {
    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        double salario,totalIR=0;
        salario = teclado.nextDouble();

        if (salario <= 2000.00) {
            System.out.println("Isento");
        } else {
            if (salario > 4500.00) {
                totalIR=(salario-4500)*28/100+1500*18/100+1000*8/100;
            } else {
                if (salario > 3000.00) {
                    totalIR=(salario-3000)*18/100+1000*8/100;                    
                } else {
                    totalIR+=(salario-2000)*8/100;;
                }
            }
            System.out.printf("R$ %.2f",totalIR);
        }
        
        
        
        teclado.close();
    }
}
