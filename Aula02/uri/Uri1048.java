package uri;

import java.util.Scanner;

public class Uri1048 {
    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        double salario,aumento=0;
        salario = teclado.nextDouble();

        if (salario <= 400.00) {
            aumento+=15;
        } else {
            if (salario <= 800.00) {
                aumento+=12;
            } else {
                if (salario <= 1200.00) {
                    aumento+=10;
                } else {
                    if (salario <= 2000.00) {
                        aumento+=7;
                    } else {
                        aumento+=4;
                    }
                }
            }
        }
        System.out.printf("Novo salario: %.2f\nReajuste ganho: %.2f\nEm percentual: %.0f %%",salario*(100+aumento)/100,salario*aumento/100,aumento);
        
        teclado.close();
    }
}
