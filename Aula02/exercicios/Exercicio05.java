package exercicios;

import java.util.Scanner;
/*
    Validar senha digitada
*/
public class Exercicio05 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String senhaReal, senhaDigitada;
        senhaReal = "R10p5";
    

        System.out.println("Informe a senha: ");
        senhaDigitada = in.nextLine();
        
        if (senhaDigitada.equalsIgnoreCase(senhaReal)){} //Ignore case sensitive!
     
        if (senhaDigitada.equals(senhaReal)){
            System.out.println("Acesso Concedido!");
        } else{
            System.out.println("Acesso Negado!");
        }
        in.close();
    }
}
