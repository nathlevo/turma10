package exercicios;

import java.util.Scanner;

public class Exercicio06 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        float salario;
        salario = entrada.nextFloat();
        if (salario <= 600.0) {
            System.out.println("Isento");
        } else {
            if (salario <= 1200.0) {
                System.out.printf("Alíquota 20%%, total de imposto R$ %.2f \n", salario * 0.2);
            } else {
                if (salario <= 2000.0) {
                    System.out.printf("Alíquota 25%%, total de imposto R$ %.2f\n", salario * 0.25);
                } else {
                    System.out.printf("Alíquota 30%%, total de imposto R$ %.2f\n", salario * 0.3);
                }
            }
        }

        entrada.close();
    }
}
