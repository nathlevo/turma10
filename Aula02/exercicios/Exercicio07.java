package exercicios;

import java.util.Scanner;

public class Exercicio07 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int a, b, c;
        System.out.println("Informe o comprimento do lado a:");
        a = entrada.nextInt();
        System.out.println("Informe o comprimento do lado b:");
        b = entrada.nextInt();
        System.out.println("Informe o comprimento do lado c:");
        c = entrada.nextInt();

        boolean naoTriangulo, trianguloEscaleno, trianguloEquilatero;

        naoTriangulo = (a > b + c) || (b > a + c) || (c > b + a);
        trianguloEscaleno = (a != b) && (b != c) && (a != c);
        trianguloEquilatero = (a == b) && (b == c);

        if (naoTriangulo) {
            System.out.println("Não é um triângulo.");
        } else {

            if (trianguloEquilatero) {
                System.out.println("Triângulo Equilátero.");
            } else {
                if (trianguloEscaleno) {
                    System.out.println("Triângulo Escaleno.");
                } else {
                    System.out.println("Triângulo Isósceles.");
                }
            }
        }
        //ALT + SHIIFT + SETA  - copiar colar
        //ALT + SETA  - mover de linha
        entrada.close();
    }
}
