package exercicios;

import java.util.Scanner;
/*
    Ler 2 números (salário e valor da prestação), o empréstimo só pode ser concedido se a mensalidade não comprometer mais que 30% do salario
*/
public class Exercicio04 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double salario, prestacao;
        

        System.out.println("Digite o salário: ");
        salario=in.nextDouble();
        System.out.println("Digite a prestação: ");
        prestacao=in.nextDouble();

     
        if (prestacao > 0.3*salario){
            System.out.println("Empréstimo rejeitado");
        } else{
            System.out.println("Empréstimo aprovado");
        }
        in.close();
    }
}
