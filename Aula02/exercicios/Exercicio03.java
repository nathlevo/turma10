package exercicios;

import java.util.Scanner;
/*
    Ler 2 números (ponto flutuante) e apresentá-los em ordem não crescente
*/
public class Exercicio03 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double nota1, nota2;
        

        System.out.println("Digite a nota 1: ");
        nota1=in.nextDouble();
        System.out.println("Digite a nota 2: ");
        nota2=in.nextDouble();

     
        if (nota1 > nota2){
            System.out.println("1- " + nota1 + "\n"+ "2- " + nota2);
        } else{
            System.out.println("1- " + nota2 + "\n"+ "2- " + nota1);
        }
        in.close();
    }
}
