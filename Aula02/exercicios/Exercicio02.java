package exercicios;

import java.util.Scanner;

public class Exercicio02 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double nota1, nota2, notaFinal;
        

        System.out.println("Digite a nota 1: ");
        nota1=in.nextDouble();
        System.out.println("Digite a nota 2: ");
        nota2=in.nextDouble();

        notaFinal = ( 0.4 * nota1 ) + ( 0.6 * nota2 );
     
        if (notaFinal >= 6.0){
            System.out.println("Aprovado com média  "+ notaFinal);
        } else{
            System.out.println("Reprovado com média  "+ notaFinal);
        }
        in.close();
    }
}
