package exemplos.exemplo01;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        ArrayList<Funcionario> funcionarios = new ArrayList<>();
        
        Gerente g = new Gerente("Nath", 5000.00,10);
        Funcionario f = new Funcionario("Joana",4000.00);

        funcionarios.add(f);
        funcionarios.add(g);

        for (Funcionario funcionario : funcionarios) {
            System.out.println(funcionario);           
            funcionario.aumentaSalario(0.1);
            System.out.println(funcionario);  
        }

        System.out.println("------");
        //g.exibeGer();
        //g.exibetudo();
        //System.out.println();
        //f.exibetudo();
        System.out.println(g);
        System.out.println(f);  
        g.aumentaSalario(0.0);
        f.aumentaSalario(0.0);
        System.out.println(g);
        System.out.println(f);  
    }
}
