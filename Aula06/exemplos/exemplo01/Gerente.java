package exemplos.exemplo01;

public class Gerente extends Funcionario{
    private int numFunc;
    public Gerente(String nome, double salario, int numFunc){        
        super(nome, salario); //Funcionario(nome, salario)
        this.numFunc=numFunc;
    }
    public void exibeGer(){
        super.exibetudo();
        /*
        System.out.println(nome);//var protected
        System.out.println(super.getsalario());//var private
        */
        System.out.println("#Funcionários: "+numFunc);
    }
    @Override //indica que está sobrecarregando
    public void exibetudo(){
        super.exibetudo();
        System.out.println("#Funcionários: "+numFunc);
    }
    @Override
    public String toString(){
        return super.toString()+" #Funcionários: "+numFunc;
    }
    @Override
    public void aumentaSalario(double perc){
        super.aumentaSalario(perc+0.2);       
    }
}
