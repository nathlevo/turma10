package exemplos.exemplo01;

/**
 * Funcionario
 */
public class Funcionario {
    protected String nome; //Funciona como público dentro da hierarquia
    private double salario;

    //Duas opções sem construtores
    /*
    public Funcionario (){}
    */
    public Funcionario (String nome, double salario){
        this.nome=nome;
        this.salario=salario;
    }
    public String getNome(){
        return nome;
    }
    public double getSalario(){
        return salario;
    }
    public void setSalario(double salario){
        this.salario=salario;
    }
    public void exibetudo(){
        System.out.printf("Nome: %s\nSalário: R$%.2f\n",nome,salario);        
    }
    @Override
    public String toString(){
        return "Nome: "+nome+" Salário: R$"+salario;
    }
    public void aumentaSalario(double perc){
        salario=salario*(1+perc);
    }
}