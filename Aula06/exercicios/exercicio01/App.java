package exercicios.exercicio01;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        ArrayList<Placar> placars = new ArrayList<>();
        placars.add(new Placar());
        placars.add(new Placar("Palmeiras","Botafogo"));
        placars.add(new Placar("Palmeiras","Botafogo",3,1));    
        for (Placar placar : placars) {
            System.out.println(placar);
        }
    }
}
