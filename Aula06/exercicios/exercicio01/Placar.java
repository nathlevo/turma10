package exercicios.exercicio01;

public class Placar {
    private int golsLocal,golsConvidado;
    private String timeLocal,timeConvidado;
    /**
     * InnerPlacar
     */
    public Placar() {
        this("Convidado","Local",0,0);
    }

    public Placar(String timeConvidado, String timeLocal) {
        this(timeConvidado,timeLocal,0,0);
        //golsConvidado=0;
        //golsLocal=0;        
        //this.timeConvidado=timeConvidado;
        //this.timeLocal=timeLocal;
    }
    public Placar(String timeConvidado, String timeLocal, int golConvidado, int golsLocal) {
        this.golsConvidado=golConvidado;
        this.golsLocal=golsLocal;
        this.timeConvidado=timeConvidado;
        this.timeLocal=timeLocal;
    }
    
    @Override
    public String toString(){
        return timeLocal+" "+golsLocal+" x "+golsConvidado+" "+timeConvidado;
    }

}
