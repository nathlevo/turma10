package exercicios.exercicio02;

public class ContaCorrente extends Conta {
    final double TAXA_DEPOSITO = 0.1;

    public ContaCorrente(int numConta) {
        super(numConta);
    }

    @Override
    public boolean debitoConta(double valorDebito){        
        if (valorDebito <= getSaldo()) {
            super.debitoConta(valorDebito);
            return true;
        }   
        return false;       
    }

    @Override
    public boolean depositoConta(double valorDeposito) {
        return super.depositoConta(valorDeposito - TAXA_DEPOSITO);
    }
}
