package exercicios.exercicio02;

public abstract class Conta {
    protected double saldoConta;
    private int numConta;

    public Conta(int numConta) {
        this.numConta = numConta;
        saldoConta = 0;
    }

    public int getNumero() {
        return numConta;
    }

    public double getSaldo() {
        return saldoConta;
    }

    public boolean depositoConta(double valorDeposito) {
        if (valorDeposito > 0) {
            this.saldoConta += valorDeposito;
            return true;
        } else {
            return false;
        }
    }

    // public abstract String debitoConta(double valorDebito){} //Obriga todos a implantarem
    public boolean debitoConta(double valorDebito) {
        this.saldoConta -= valorDebito;  
        return true;                  
    }

    @Override
    public String toString() {
        return "Conta: " + numConta + " | Saldo: R$ " + saldoConta;
    }
}
