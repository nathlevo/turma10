package exercicios.exercicio02;

public class ContaEspecial extends Conta{
    private double contaEspLimite;
    public ContaEspecial(int numConta, double contaEspLimite){
        super(numConta);
        this.contaEspLimite=contaEspLimite;
    }
    @Override
    public boolean debitoConta(double valorDebito){        
        if (valorDebito <= getSaldo()+contaEspLimite) {
            super.debitoConta(valorDebito);
            return true;
        }   
        return false;       
    }
    @Override
    public String toString() {
        return super.toString()+" | Limite: R$ " + contaEspLimite;
    }
}