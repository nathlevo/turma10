package exercicios.exercicio02;

import java.util.ArrayList;

public class GerenciaConta {
    private ArrayList<Conta> contas = null;
    public GerenciaConta(){
        contas = new ArrayList<>();
    }
    public void novaContaCorrente(int numeroConta){        
        contas.add(new ContaCorrente(numeroConta));
    }
    public void novaContaEspecial(int numeroConta,double limite){        
        contas.add(new ContaEspecial(numeroConta,limite));
    }
    public void novaContaPoupanca(int numeroConta){        
        contas.add(new ContaPoupanca(numeroConta));
    }
    public boolean novoDepositoConta(int numeroConta, double valor){
        for (Conta conta : contas) {
            if (conta.getNumero() == numeroConta) {                         
                return conta.depositoConta(valor);                
            }
        } 
        return false; 
    }
    public boolean novoDebitoConta(int numeroConta, double valor){
        for (Conta conta : contas) {
            if (conta.getNumero() == numeroConta) {
                return conta.debitoConta(valor);                               
            }            
        }
        return false;
    }
    public String novoConsultaConta(int numeroConta){
        for (Conta conta : contas) {
            if (conta.getNumero() == numeroConta) {
                return conta.toString();
                //return true;                
            }
        }
        return "Conta não encontrada.";
    }
}
