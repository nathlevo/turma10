package exercicios.exercicio02;

import java.util.Scanner;

public class AppConta {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        double limite,valor;
        int numeroConta;
        int opcao;

        GerenciaConta listaDeContas = new GerenciaConta();

        boolean resultadoOp=false;
        do {
            System.out.println("1-Nova Conta Corrente");
            System.out.println("2-Nova Conta Especial");
            System.out.println("3-Nova Conta Poupança");
            System.out.println("4-Realizar Depósito");
            System.out.println("5-Realizar Saque");
            System.out.println("6-Consultar Saldo");
            System.out.println("7-Sair");
            opcao = teclado.nextInt();
            switch (opcao) {
                case 1:
                //Cria CC
                    System.out.println("Informe o número da conta!");
                    numeroConta = teclado.nextInt();

                    listaDeContas.novaContaCorrente(numeroConta);
                    break;
                case 2:
                //Cria CE
                    System.out.println("Informe o número da conta!");
                    numeroConta = teclado.nextInt();
                    System.out.println("Informe o limite: ");
                    limite = teclado.nextDouble();
                    
                    listaDeContas.novaContaEspecial(numeroConta, limite);
                    break;                
                case 3:
                //Cria CP
                    System.out.println("Informe o número da conta!");
                    numeroConta = teclado.nextInt();                    
                    
                    listaDeContas.novaContaPoupanca(numeroConta);
                    break;
                case 4:
                //Deposito
                    System.out.println("Informe o número da conta:");
                    numeroConta=teclado.nextInt();
                    System.out.println("Informe o valor do depósito: ");
                    valor=teclado.nextDouble();

                    resultadoOp=listaDeContas.novoDepositoConta(numeroConta, valor);                  

                    if (resultadoOp==false) {
                        System.out.println("Operação não realizada.");   
                    } else {
                        System.out.println("Operação realizada.");   
                    } 
                    break;
                case 5:
                //Saque
                    System.out.println("Informe o número da conta:");
                    numeroConta=teclado.nextInt();
                    System.out.println("Informe o valor do saque: ");
                    valor=teclado.nextDouble();

                    resultadoOp = listaDeContas.novoDebitoConta(numeroConta, valor);
                    
                    if (resultadoOp==false) {
                        System.out.println("Operação não realizada.");   
                    } else {
                        System.out.println("Operação realizada.");   
                    } 
                                       
                    break;
                case 6:
                //Consulta Saldo                             
                    System.out.println("Informe o número da conta:");
                    numeroConta=teclado.nextInt();        
                    
                    String resultado = listaDeContas.novoConsultaConta(numeroConta);
                    
                    System.out.println(resultado);
                    break;
                case 7:
                    System.out.println("Tchau!!");
                    break;
                default:
                    System.out.println("Opção Inválida!");
                    break;
            }
        } while (opcao != 7);
        teclado.close();
    }
}
