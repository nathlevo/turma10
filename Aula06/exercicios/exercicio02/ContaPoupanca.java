package exercicios.exercicio02;

public class ContaPoupanca extends Conta{ 
    private static double taxaPoup;   //Atributo da classe, comum a todos os objetos
    public ContaPoupanca(int numConta){
        super(numConta);        
    }
    public static void setTaxa(double taxa){
        taxaPoup=taxa;
    }
    public static double getTaxa(){
        return taxaPoup;
    }

    @Override
    public boolean debitoConta(double valorDebito){        
        if (valorDebito+taxaPoup <= getSaldo()) {
            super.debitoConta(valorDebito+taxaPoup);
            return true;
        }   
        return false;       
    }    
}