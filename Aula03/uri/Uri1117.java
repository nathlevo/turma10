package uri;

import java.util.Scanner;

/**
 * Exercicio01
 */
public class Uri1117 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        double nota=0,media=0;
        int i=0;
        while (i<2) {
            nota=entrada.nextDouble();
            if (nota>=0.0 && nota<=10.0) {
                i++;
                media=media+nota;
            }else{
                System.out.println("nota invalida");
            }
        }
        System.out.printf("media = %.2f",media/2);
        entrada.close();        
    }
}