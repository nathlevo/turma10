package uri;

import java.util.Scanner;

/**
 * Exercicio01
 */
public class Uri1066 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int num, positivos = 0, pares = 0, negativos = 0;

        for (int i = 0; i < 5; i++) {
            num = entrada.nextInt();
            if (num > 0) {
                positivos++;
            } else {
                if (num < 0) {
                    negativos++;
                }
            }
            if (num % 2 == 0) {
                pares++;
            }
        }
        System.out.println(pares + " valor(es) par(es)");
        System.out.println((5 - pares) + " valor(es) impar(es)");
        System.out.println(positivos + " valor(es) positivo(s)");
        System.out.println(negativos + " valor(es) negativo(s)");
        entrada.close();
    }

}