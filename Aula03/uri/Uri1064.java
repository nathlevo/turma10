package uri;

import java.util.Scanner;

/**
 * Exercicio01
 */
public class Uri1064 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        double num,media=0;
        int positivos=0;
        for (int i = 0; i < 6; i++) {
            num=entrada.nextDouble();
            if (num>0) {
                positivos++;
                media=media+num;
            } 
        }
        System.out.println(positivos+" valores positivos");
        System.out.printf("%.1f\n",media/positivos);
        entrada.close();
    }

}