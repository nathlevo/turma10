package exercicios;

import java.util.Scanner;

/**
 * Exercicio02
 */
public class Exercicio02 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int contador = 0, valor;
        System.out.println("Informe um número: ");
        valor = entrada.nextInt();

        System.out.println("Valor digitado: " + valor);

        while (contador <= 10) {
            System.out.printf("\n%d x %d = %d", valor, contador, valor * contador);
            contador++;
        }

        entrada.close();
    }

}