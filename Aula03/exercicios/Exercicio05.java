package exercicios;

import java.util.Scanner;

/**
 * Exercicio02
 */
public class Exercicio05 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int valor, par = 0, impar = 0;

        for (int i = 1; i < 11; i++) {
            System.out.print("Digite o " + i + "º número: ");
            valor = entrada.nextInt();
            // System.out.println();
            if (valor % 2 == 1) {
                impar++;
            } else {
                par++;
            }
        }
        System.out.println();
        System.out.println("O total de pares é: " + par);
        System.out.println("O total de ímpares é: " + impar);
        entrada.close();
    }

}