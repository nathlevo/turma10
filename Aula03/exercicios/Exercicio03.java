package exercicios;

import java.util.Scanner;

/**
 * Exercicio02
 */
public class Exercicio03 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int valor, resultado=2;
        System.out.print("Digite um número: ");
        valor = entrada.nextInt();
        System.out.printf("Sequência impressa: %d",1);
        while (resultado <= valor) {
            
            System.out.printf(", %d",resultado);               
            resultado *= 2;  
        }

        entrada.close();
    }

}