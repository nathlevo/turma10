package exercicios;

import java.util.Scanner;

/**
 * Exercicio02
 */
public class Exercicio07 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        final int totalTurmas = 3, totalAlunos = 2;
        int i = 1, j = 1;
        double somaMedias = 0, mediaAluno, mediaGeral = 0;

        while (i <= totalTurmas) {
            System.out.println("TURMA " + i);
            j = 1;
            somaMedias = 0;
            while (j <= totalAlunos) {
                System.out.print("Digite a média do " + j + "º aluno da turma " + i + ": ");
                mediaAluno = entrada.nextDouble();
                somaMedias = somaMedias + mediaAluno;
                j++;
            }
            System.out.println("A média da turma " + i + " é " + somaMedias / (double) (j - 1));
            mediaGeral = mediaGeral + somaMedias / (double) (j - 1);
            i++;
        }
        System.out.println("A média geral é " + mediaGeral/totalTurmas);
        entrada.close();
    }

}