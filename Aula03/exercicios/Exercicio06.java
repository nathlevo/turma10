package exercicios;

import java.util.Scanner;

/**
 * Exercicio02
 */
public class Exercicio06 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int valor, par = 0, somaPar = 0;

        for (int i = 1; i < 11; i++) {
            System.out.print("Digite o " + i + "º número: ");
            valor = entrada.nextInt();
            // System.out.println();
            if (valor % 2 == 0) {
                par++;
                somaPar=somaPar+valor;
            } 
        }
        System.out.println();
        System.out.println("Média pares é: " + somaPar/par);
        System.out.println("O porcentagem de ímpares é: " + (100-10*par) + "%");
        entrada.close();
    }

}