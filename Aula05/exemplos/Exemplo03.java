package exemplos;

import java.util.HashSet;

public class Exemplo03 {
    public static void main(String[] args) {
        HashSet<String> carros = new HashSet<>();
        carros.add("Ka");
        carros.add("Fusca");
        carros.add("Corsa");
        carros.add("Palio");
        carros.add("HB20");
        carros.add("Palio");
        System.out.println(carros);
        for (String item : carros) {
            System.out.println(item);
        }
    }
}
