package exemplos;

import java.util.HashMap;

/**
 * Exemplo01
 */
public class Exemplo02 {
    public static void main(String[] args) {
        HashMap<Integer,String> clientes = new HashMap<>();
        clientes.put(1,"Nathalia");
        clientes.put(2,"Alexandre");
        System.out.println("Cliente 1: "+clientes.get(1));
        System.out.println(clientes);
    }        
}