package exemplos;
import java.util.ArrayList;
/**
 * Exemplo01
 */
public class Exemplo01 {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<>(); //cria lista dinâmica para armazenar inteiros
        ArrayList<String> nomes = new ArrayList<>();
        nomes.add("Nathalia");
        nomes.add("Juliana");
        lista.add(3);
        lista.add(5);
        lista.add(6);
        System.out.println("Qtidade de itens: "+lista.size());
        System.out.println("Primeiro elemento da lista: " + lista.get(1));
        lista.add(1,10);
        System.out.println("Primeiro elemento da lista: " + lista.get(1));
        for (int i = 0; i < lista.size();i++) {
            System.out.print(" " + lista.get(i));
        }
        System.out.println();
        for (Integer item : lista) {
            System.out.print(" " + item);
        }
        System.out.println();
        for (String item : nomes) {
            System.out.println(item);            
        }
    }        
}