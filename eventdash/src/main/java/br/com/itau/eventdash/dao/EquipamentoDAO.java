package br.com.itau.eventdash.dao;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.eventdash.model.Equipamento;

public interface EquipamentoDAO extends CrudRepository<Equipamento, Integer>{
	
	
}
