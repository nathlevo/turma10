package br.com.itau.eventdash.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.eventdash.dao.AlarmeDAO;
import br.com.itau.eventdash.model.Alarme;

@CrossOrigin(origins = "*")
@RestController
public class AlarmeController {

	@Autowired
	private AlarmeDAO dao;

	@GetMapping("/alarmes")
	public ResponseEntity<ArrayList<Alarme>> listaAlarmes() {
		ArrayList<Alarme> ListaAlarmes = (ArrayList<Alarme>) dao.findAll();
		return ResponseEntity.ok(ListaAlarmes);
	}
}
