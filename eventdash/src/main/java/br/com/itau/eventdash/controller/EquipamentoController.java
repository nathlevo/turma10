package br.com.itau.eventdash.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.eventdash.dao.EquipamentoDAO;
import br.com.itau.eventdash.model.Equipamento;

@CrossOrigin(origins = "*")
@RestController
public class EquipamentoController {

	@Autowired
	private EquipamentoDAO dao;

	@GetMapping("/equipamentos")
	public ResponseEntity<ArrayList<Equipamento>> listaEquipamentos() {
		ArrayList<Equipamento> ListaEquipamentos = (ArrayList<Equipamento>) dao.findAll();
		return ResponseEntity.ok(ListaEquipamentos);
	}
}
